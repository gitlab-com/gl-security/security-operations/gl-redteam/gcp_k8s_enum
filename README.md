# gcp_k8s_enum

This tool analyzes the output of several `gcloud` commands to determine services exposed to the public Internet via Google Kubernetes Engine (GKE) [Ingress](https://cloud.google.com/kubernetes-engine/docs/concepts/ingress).

Google provides very nice centralized tooling that already does this, in the form of [Asset Inventory](https://cloud.google.com/asset-inventory/docs/quickstart) and [Security Command Center](https://cloud.google.com/security-command-center). The scripts here do not leverage these tools, assuming they are either not enabled or that you are working with accounts lacking the required permissions to use them.

## Usage

### General Usage

You can use this tool to analyze a single GCP project or many projects at once.

The gcp_k8s_enum.py python script performs offline analysis only. You'll need the proper permissions to export the data via gcloud prior to running.

To gather the initial data, use the provided bash script as follows:

```
# Gather data for a single project
./gather-data.sh [PROJECT_ID]

# Gather data for all visible projects
./gather-data.sh

# Gather data for all visible projects, ignore a regex pattern of projects
IGNORE="test-project-*" ./gather-data.sh
```

The script will create a new directory called `k8s-data` in your working folder, with a hierarchy like this:

```
- k8s-data
  - [PROJECT-ID]
    - [GKE-CLUSTER-NAME].json (machine-readable output)
    - [GKE-CLUSTER-NAME].txt (human-readable output)
```

That may be enough for you. However, if you then want to consolidate all of that information into a single CSV file and/or load it into BigQuery for analysis, you can use the included python script as follows:

```
# Just generate the CSV
./gcp_k8s_enum.py --input ./k8s-data

# Generate CSV and load into BigQuery
./gcp_k8s_enum.py --input ./k8s-data --bigquery [PROJECT-ID]
```

**IMPORTANT**<br>
This tool will leave k8s credential data in the home folder of the user who runs the collection script. You may want to securely delete that info (~/.kube/config) when you are done.

### Automating with GitLab

You can run this as a GitLab CI job! You need to set the following CI variables:

- File `KEY_FILE`: Required. Service account key file contents to activate in GCP.
- Variable `IGNORE`: Optional. Python-compatible regex of projects to exclude.
- Variable `PROJECT_ID`: Optional. GCP project for BigQuery database persistence.

Output files will be available in the project artifacts for 1 week. Database persistence can also be used to integrate with other tools.

### BigQuery Setup

This tool can also upload data to BigQuery. We do this ourselves to feed the Red Team's bot, which performs some automated attack simulations.

```
PROJECT=[GCP PROJECT ID]
# Setup the resources dataset
bq --location=US mk \
--dataset \
--description "Known GCP resources (refreshed nightly)" \
"$PROJECT":"resources"

bq mk \
--table \
"$PROJECT":"resources"."details" \
id:STRING,name:STRING,resourceType:STRING,externalAddress:STRING,allowedTCP:STRING,allowedUDP:STRING,identifier:STRING,gitlabURL:STRING
```

## Design Decisions

At GitLab, this data ultimately feeds into a bot that is also analyzing the output of compute instance information. Because of this, some database fields (such as allowedTCP, allowedUDP) may seem a bit out of place. They are there to maintain compatibility with some database functions.